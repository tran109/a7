<?php
    session_start();
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
         <meta name="viewport" content="width=device-width">
        <title>Address</title>
    </head>
    
    <body>
        <?php
        //Store data in the $_SESSION superglobal
        $_SESSION["firstName"]=$_GET["firstName"];
        $_SESSION["lastName"]=$_GET["lastName"];
        $_SESSION["email"]=$_GET["email"];
        $_SESSION["birthDate"]=$_GET["birthDate"];
        $_SESSION["runner"]=$_GET["runner"];
        $_SESSION["terrain"]=$_GET["terrain"];
        $_SESSION["rank"]=$_GET["rank"];  
//        //Check if this is a new session or an old one
//        if (isset($_SESSION)){
//        echo "new session";}
//        else{
//        echo "old session";}

        ?>
        
        <h1>Address Page</h1> 
        <h2>Hello <?= $_SESSION["firstName"] ?></h2>
        
        <form action="shipping.php" method="GET">
            <label for="address">Address:</label>
            <input type="text" name="address" maxlength="100" required><br>  
            
            <label for="city">City:</label>
            <input type="text" name="city" maxlength="20" required><br>  
            
            <label for="province">Province:</label>
                    <select name="province">
			<option value="ON">ON</option>
                        <option value="QC">QC</option>
                        <option value="AB">AB</option>
                        <option value="BC">BC</option>
                        <option value="MB">MB</option>
                        <option value="NL">NL</option>
                    </select><br>
                      
            <label for="postal">Postal Code:</label>
            <input type="text" name="postal" maxlength="7" pattern="[A-Za-z][0-9][A-Za-z] [0-9][A-Za-z][0-9]" required><br> 
            
            <input type="submit" value="Submit">
        </form>
    </body>
</html>
