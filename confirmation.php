<?php
    session_start();
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
         <meta name="viewport" content="width=device-width">
        <title>Confirmation</title>
    </head>
    
    <body>
        <?php
        //Store data in the $_SESSION superglobal
         $_SESSION["ship"]=$_GET["ship"];
        ?>

        <h1>Confirmation Page</h1>
        
        <ul><h2>Customer Details</h2>
            <li>First name: <?= $_SESSION["firstName"] ?></li>
            <li>Last name: <?= $_SESSION["lastName"] ?></li>
            <li>Email: <?=$_SESSION["email"] ?></li>
            <li>Date: <?= $_SESSION["birthDate"] ?></li>
            <li>Runner: <?=$_SESSION["runner"] ?></li>
            <li>Favourite terrain: <?=$_SESSION["terrain"] ?></li>
            <li>Rate your shoes: <?=$_SESSION["rank"] ?></li>
        </ul>
       
        <ul><h2>Address Details</h2>
            <li>Address: <?=$_SESSION["address"] ?></li>
            <li>City: <?=$_SESSION["city"] ?></li>
            <li>Province: <?=$_SESSION["province"] ?></li>
            <li>Postal code: <?=$_SESSION["postal"] ?></li>
        </ul>
        
        <ul><h2>Shipping Details</h2>
            <li>Shipping method: <?=$_SESSION["ship"] ?></li>
        </ul>
        
         <form action="completed.php" method="GET">
             <input type="submit" value="Submit">
         </form>
    </body>
</html>
