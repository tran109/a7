<?php
    session_start();
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width">
        <title>Courier</title>
    </head>
    
    <body>
        <?php
        $_SESSION["address"]=$_GET["address"];
        $_SESSION["city"]=$_GET["city"];
        $_SESSION["province"]=$_GET["province"];
        $_SESSION["postal"]=$_GET["postal"];
        ?>
        
        <h1>Shipping Page</h1>
        
        <form action="confirmation.php"  method="GET">
            <input type="radio" name="ship" value="Canada Post" >
            <label for="canPost">Canada Post</label><br>
            <input type="radio" name="ship" value="FedEx" >
            <label for="fed">FedEx</label><br>
            <input type="radio" name="ship" value="UPS" >
            <label for="ups">UPS</label><br>
            
            <input type="submit" value="Submit">
        </form>      
    </body>
</html>
