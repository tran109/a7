<?php
    session_start();
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
         <meta name="viewport" content="width=device-width">
        <title>Main page</title>
    </head>
    
    <body>
        <h1>Customer Details</h1>
         <form action="address.php" method="GET">
            <label for="firstName">First Name:</label>
            <input type="text" name="firstName" maxlength="50" required><br>   
            
            <label for="lastName">Last Name:</label>
            <input type="text" name="lastName" maxlength="50" required><br>  
            
            <label for="email">Email:</label>
            <input type="email" name="email" required><br>  
            
            <label for="birthDate">Date of Birth:</label>
            <input type="date" name="birthDate" required><br>  
            
            <label for="runner">Are you a runner:</label>
            <input type="checkbox" name="runner" value="Yes" >Yes
            <input type="checkbox" name="runner" value="No" >No<br> 
            
            <label for="terrain">Your favourite terrain:</label><br>
            <input type="radio" name="terrain" value="Road" >
            <label for="terrain">Road</label><br>
            <input type="radio" name="terrain" value="Track" >
            <label for="terrain">Track</label><br>
            <input type="radio" name="terrain" value="Trail" >
            <label for="terrain">Trail</label><br>
            
            <label for="rank">Rank your current shoes from 1 to 10:</label>
            <input type="number" name="rank" min="1" max="10"><br>
            
            <input type="submit" value="Submit">
         </form>
    </body>
</html>
    