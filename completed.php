<?php
    session_start();
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
         <meta name="viewport" content="width=device-width">
        <title>Completed</title>
    </head>
    <body>
        <h1>Order successfully placed</h1>
        <h2> Thank you for your order!</h2>
        
         <?php
            echo "Session data destroyed<br>";
            session_destroy();
        ?>

        <a href="http://tran109.dev.fast.sheridanc.on.ca/SYST10199/A7/index.php">Home Page</a>
    </body>
</html>
